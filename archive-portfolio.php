<?php
/**
 * The template for displaying the Portfolio archive page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package StanleyWP
 */
get_header(); ?>

	<div class="container">
			<div id="primary" class="content-area-full">
				<main id="main" class="site-main row" role="main">

				<?php
				if ( have_posts() ) : ?>

					<header class="page-header col-md-12">
					<?php $portfolio_title = get_theme_mod( 'portfolio_title', 'Portfolio' ); ?>
 
					<?php if( $portfolio_title != '') : ?>
						<h1><?php echo $portfolio_title; ?></h1>
					<?php endif; ?>
					</header><!-- .page-header -->

					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/content', 'portfolio' );
					endwhile; ?>

					<div class="col-md-12">

						<?php the_posts_navigation(); ?>

					</div><!--  .col-md-12 -->

				<?php else :
					get_template_part( 'template-parts/content', 'none' );
				endif; ?>

				</main><!-- #main -->
			</div><!-- #primary -->
	</div>

<?php
get_footer();